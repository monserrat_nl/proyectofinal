var btnAbrirPopupAC1= document.getElementById('btn-abrir-popupA1'), 
    overlayAC1 = document.getElementById('overlayA1'),
    popupAC1 = document.getElementById('popupA1'),
    btnCerrarPopupAC1 = document.getElementById('btn-cerrar-popupA1');

btnAbrirPopupAC1.addEventListener('click', function()
{
    overlayAC1.classList.add('active');
    popupAC1.classList.add('active');
});

btnCerrarPopupAC1.addEventListener('click', function()
{
    overlayAC1.classList.remove('active');
    popupAC1.classList.remove('active');
});

/*segundo boton*/
var btnAbrirPopupAC2 = document.getElementById('btn-abrir-popupA2'), 
    overlayAC2 = document.getElementById('overlayA2'),
    popupAC2 = document.getElementById('popupA2'),
    btnCerrarPopupAC2 = document.getElementById('btn-cerrar-popupA2');

btnAbrirPopupAC2.addEventListener('click', function()
{
    overlayAC2.classList.add('active');
    popupAC2.classList.add('active');
});

btnCerrarPopupAC2.addEventListener('click', function()
{
    overlayAC2.classList.remove('active');
    popupAC2.classList.remove('active');
});


/*tercer boton*/
var btnAbrirPopupAC3= document.getElementById('btn-abrir-popupA3'), 
    overlayAC3 = document.getElementById('overlayA3'),
    popupAC3 = document.getElementById('popupA3'),
    btnCerrarPopupAC3 = document.getElementById('btn-cerrar-popupA3');

btnAbrirPopupAC3.addEventListener('click', function()
{
    overlayAC3.classList.add('active');
    popupAC3.classList.add('active');
});

btnCerrarPopupAC3.addEventListener('click', function()
{
    overlayAC3.classList.remove('active');
    popupAC3.classList.remove('active');
});

/*cuarto boton*/
var btnAbrirPopupAC4 = document.getElementById('btn-abrir-popupA4'), 
    overlayAC4 = document.getElementById('overlayA4'),
    popupAC4 = document.getElementById('popupA4'),
    btnCerrarPopupAC4 = document.getElementById('btn-cerrar-popupA4');

btnAbrirPopupAC4.addEventListener('click', function()
{
    overlayAC4.classList.add('active');
    popupAC4.classList.add('active');
});

btnCerrarPopupAC4.addEventListener('click', function()
{
    overlayAC4.classList.remove('active');
    popupAC4.classList.remove('active');
});

/*quinto boton*/
var btnAbrirPopupAC5 = document.getElementById('btn-abrir-popupA5'), 
    overlayAC5 = document.getElementById('overlayA5'),
    popupAC5 = document.getElementById('popupA5'),
    btnCerrarPopupAC5 = document.getElementById('btn-cerrar-popupA5');

btnAbrirPopupAC5.addEventListener('click', function()
{
    overlayAC5.classList.add('active');
    popupAC5.classList.add('active');
});

btnCerrarPopupAC5.addEventListener('click', function()
{
    overlayAC5.classList.remove('active');
    popupAC5.classList.remove('active');
});

/*sexto boton*/
var btnAbrirPopupAC6 = document.getElementById('btn-abrir-popupA6'), 
    overlayAC6 = document.getElementById('overlayA6'),
    popupAC6 = document.getElementById('popupA6'),
    btnCerrarPopupAC6 = document.getElementById('btn-cerrar-popupA6');

btnAbrirPopupAC6.addEventListener('click', function()
{
    overlayAC6.classList.add('active');
    popupAC6.classList.add('active');
});

btnCerrarPopupAC6.addEventListener('click', function()
{
    overlayAC6.classList.remove('active');
    popupAC6.classList.remove('active');
});

/*septimo boton*/
var btnAbrirPopupAC7 = document.getElementById('btn-abrir-popupA7'), 
    overlayAC7 = document.getElementById('overlayA7'),
    popupAC7 = document.getElementById('popupA7'),
    btnCerrarPopupAC7 = document.getElementById('btn-cerrar-popupA7');

btnAbrirPopupAC7.addEventListener('click', function()
{
    overlayAC7.classList.add('active');
    popupAC7.classList.add('active');
});

btnCerrarPopupAC7.addEventListener('click', function()
{
    overlayAC7.classList.remove('active');
    popupAC7.classList.remove('active');
});

/*octavo boton*/
var btnAbrirPopupAC8 = document.getElementById('btn-abrir-popupA8'), 
    overlayAC8 = document.getElementById('overlayA8'),
    popupAC8 = document.getElementById('popupA8'),
    btnCerrarPopupAC8 = document.getElementById('btn-cerrar-popupA8');

btnAbrirPopupAC8.addEventListener('click', function()
{
    overlayAC8.classList.add('active');
    popupAC8.classList.add('active');
});

btnCerrarPopupAC8.addEventListener('click', function()
{
    overlayAC8.classList.remove('active');
    popupAC8.classList.remove('active');
});

/*noveno boton*/
var btnAbrirPopupAC9 = document.getElementById('btn-abrir-popupA9'), 
    overlayAC9 = document.getElementById('overlayA9'),
    popupAC9 = document.getElementById('popupA9'),
    btnCerrarPopupAC9 = document.getElementById('btn-cerrar-popupA9');

btnAbrirPopupAC9.addEventListener('click', function()
{
    overlayAC9.classList.add('active');
    popupAC9.classList.add('active');
});

btnCerrarPopupAC9.addEventListener('click', function()
{
    overlayAC9.classList.remove('active');
    popupAC9.classList.remove('active');
});

/*decimo boton*/
var btnAbrirPopupAC10 = document.getElementById('btn-abrir-popupA10'), 
    overlayAC10 = document.getElementById('overlayA10'),
    popupAC10 = document.getElementById('popupA10'),
    btnCerrarPopupAC10 = document.getElementById('btn-cerrar-popupA10');

btnAbrirPopupAC10.addEventListener('click', function()
{
    overlayAC10.classList.add('active');
    popupAC10.classList.add('active');
});

btnCerrarPopupAC10.addEventListener('click', function()
{
    overlayAC10.classList.remove('active');
    popupAC10.classList.remove('active');
});

/*A11 boton*/
var btnAbrirPopupAC11 = document.getElementById('btn-abrir-popupA11'), 
    overlayAC11 = document.getElementById('overlayA11'),
    popupAC11 = document.getElementById('popupA11'),
    btnCerrarPopupAC11 = document.getElementById('btn-cerrar-popupA11');

btnAbrirPopupAC11.addEventListener('click', function()
{
    overlayAC11.classList.add('active');
    popupAC11.classList.add('active');
});

btnCerrarPopupAC11.addEventListener('click', function()
{
    overlayAC11.classList.remove('active');
    popupAC11.classList.remove('active');
});

/*A12 boton*/
var btnAbrirPopupAC12 = document.getElementById('btn-abrir-popupA12'), 
    overlayAC12 = document.getElementById('overlayA12'),
    popupAC12 = document.getElementById('popupA12'),
    btnCerrarPopupAC12 = document.getElementById('btn-cerrar-popupA12');

btnAbrirPopupAC12.addEventListener('click', function()
{
    overlayAC12.classList.add('active');
    popupAC12.classList.add('active');
});

btnCerrarPopupAC12.addEventListener('click', function()
{
    overlayAC12.classList.remove('active');
    popupAC12.classList.remove('active');
});

/*A13 boton*/
var btnAbrirPopupAC13 = document.getElementById('btn-abrir-popupA13'), 
    overlayAC13 = document.getElementById('overlayA13'),
    popupAC13 = document.getElementById('popupA13'),
    btnCerrarPopupAC13 = document.getElementById('btn-cerrar-popupA13');

btnAbrirPopupAC13.addEventListener('click', function()
{
    overlayAC13.classList.add('active');
    popupAC13.classList.add('active');
});

btnCerrarPopupAC13.addEventListener('click', function()
{
    overlayAC13.classList.remove('active');
    popupAC13.classList.remove('active');
});

/*A14 boton*/
var btnAbrirPopupAC14 = document.getElementById('btn-abrir-popupA14'), 
    overlayAC14= document.getElementById('overlayA14'),
    popupAC14= document.getElementById('popupA14'),
    btnCerrarPopupAC14 = document.getElementById('btn-cerrar-popupA14');

btnAbrirPopupAC14.addEventListener('click', function()
{
    overlayAC14.classList.add('active');
    popupAC14.classList.add('active');
});

btnCerrarPopupAC14.addEventListener('click', function()
{
    overlayAC14.classList.remove('active');
    popupAC14.classList.remove('active');
});

/*A15 boton*/
var btnAbrirPopupAC15 = document.getElementById('btn-abrir-popupA15'), 
    overlayAC15 = document.getElementById('overlayA15'),
    popupAC15 = document.getElementById('popupA15'),
    btnCerrarPopupAC15 = document.getElementById('btn-cerrar-popupA15');

btnAbrirPopupAC15.addEventListener('click', function()
{
    overlayAC15.classList.add('active');
    popupAC15.classList.add('active');
});

btnCerrarPopupAC15.addEventListener('click', function()
{
    overlayAC15.classList.remove('active');
    popupAC15.classList.remove('active');
});

/*1AA6 boton*/
var btnAbrirPopupAC16 = document.getElementById('btn-abrir-popupA16'), 
    overlayAC16 = document.getElementById('overlayA16'),
    popupAC16 = document.getElementById('popupA16'),
    btnCerrarPopupAC16 = document.getElementById('btn-cerrar-popupA16');

btnAbrirPopupAC16.addEventListener('click', function()
{
    overlayAC16.classList.add('active');
    popupAC16.classList.add('active');
});

btnCerrarPopupAC16.addEventListener('click', function()
{
    overlayAC16.classList.remove('active');
    popupAC16.classList.remove('active');
});