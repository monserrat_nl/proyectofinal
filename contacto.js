function NombreVacio()
{
    var nom = document.getElementById("nombre").value;
    if (nom.length == 0)
        window.alert('El campo del nombre no debe quedar vacío.');
}
function CorreoVacio()
{
    var correo = document.getElementById("correo").value;
    if (correo.length == 0)
    {
        window.alert('El campo del correo electrónico no debe quedar vacío.');
    }  
    else 
    {
        var correoValidado = document.getElementById('correo').value;
        var arroba = correoValidado.indexOf('@');
        var posCom = correoValidado.indexOf('.com');
        var posMx = correoValidado.indexOf('.mx');
        var posNet = correoValidado.indexOf('.net');
        var iframeCorreo = verificadorCorreo.document;
        
        if(verificadorCorreo.contentDocument)
            iframeCorreo = verificadorCorreo.contentDocument;
        else{
            if (verificadorCorreo.contentWindow)
                iframeCorreo = verificadorCorreo.contentWindow.document;
            }
        if (arroba == -1)
        {
            if(iframeCorreo){
                iframeCorreo.open();
                iframeCorreo.writeln('El correo no es válido.');
                iframeCorreo.close();
                window.alert('Datos erróneos.');
            }
            else 
                alert('Error al enviar el formulario.')
        }
        if ((posCom == -1) && (posNet == -1) && (posMx == -1))
        {
            if(iframeCorreo){
                iframeCorreo.open();
                iframeCorreo.writeln('El correo no es válido.');
                iframeCorreo.close();
                window.alert('Datos erróneos.');
            }
            else 
                alert('Error al enviar el formulario.')
        }
    }
}
function MensajeVacio()
{   
    var mensaje = document.getElementById("mensaje").value;
    if (mensaje.length == 0)
        window.alert('El campo del mensaje no debe quedar vacío.');
}

function send_form()
{
    var ventana = window.confirm('¿Está seguro que desea enviar el formulario?');
    if(ventana)
    {          
        window.location = "gracias.html";
        return true;
    }
    else
    {
        return false;
    }
}